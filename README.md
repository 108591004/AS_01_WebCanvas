# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y         |


---

### How to use 

盡量以全螢幕操作，有些地方做得不夠好，會被遮住。

### Function description

可以決定圖形為實心或空心，字體支援非常多種。
顏色粗細自由調整。

### Gitlab page link

    https://gitlab.com/108591004/AS_01_WebCanvas.git

### Others (Optional)

新增文字的按鈕圖示不見了，在GitLab中是有圖片檔的。

<style>
table th{
    width: 100%;
}
</style>
